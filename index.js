/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
//import renderer from "react-test-renderer";
// it("renders correctly", () => {
//     const tree = renderer.create(<App />);
//     expect(tree).toMatchSnapshot();
// });
AppRegistry.registerComponent(appName, () => App);
