**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

## 로컬 start 관련
react-native run-android

## cache 관련
npm cache clena --force
npm react-native start --reset-cache

## [이슈]
- native-base => 2.14.x 이상 upgrade시 datePicker 싱크 안맞음
- react-native => 0.64.x 이상 upgrade시 hermes 설치 및 옵션 추가해야할거 많음 (에러 이슈 많음, 안정화 될때까지 지양)
- mobx와 mobx-react는 update시 버전 체크 아직 Mobx 6이상, 혹은 mobx-react 7이상 올리면 좀 에러남
- 추가적으로 React Version 역시 17 업데이트시 mobx 현재버전과 충돌
-   [Mobx-react 설치 유의사항]
    NPM 버전    MobX 버전    지원	    
    v7	        6. *	    16.8 이상
    v6	        4._ / 5._	16.8 이상
- npm v7 이상일 경우, react-native-svg 가 install 되지 않음, 6으로 Downgrade (npm -g install npm@6.14.11)

## 개발환경
- OS : Window 10
- Tool : Visual Studio Code / Android Studio
- Npm v7.6.3 => Npm v6.14.11 / Node v14.16.0

watchman watch-del-all
rm -fr $TMPDIR/react-*
react-native start --reset-cache
 
Or,
 
rm -rf node_modules
yarn
react-native start --reset-cache


## TODO
- [1Page]
- 디자인 입히기
- 세션 유지가능한지 찾아보기

- [2Page]
- 디자인 입히기

- [3Page]
- 디자인 입히기
- 누적 적립금
- x회차 적립일

- [4page]
- 게시판

- [기타]
- 광고 추가
- DB연동

# [keystore 등록방법] 및 앱 빌드 방법
1. ./android/app 에서 아래의 명령어를 입력한다.
- keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000

2. ./android 에서 gradlew assembleRelease 를 입력한다
3. 직접 디바이스 실행 법 : cd.. -> react-native run-android --variant=release
- 3)의 경우, 안드로이드에서 현재 깔려있는 apk를 제거 후 위의 명령어를 실행한다.


- 해시키
- Xo8WBi6jzSxKDVR4drqm84yr9iU=
- 관리자 >> L9H9t5*D!i%b
- 이름등등 soyoonjeong
- 국가코드 KR