import {observable, action, toJS, computed} from 'mobx';
import moment from 'moment';

class Storage {
  //초기 데이터
  @observable _initData = {
    // startDay: 'YYYY-MM-DD',
    startDay: moment().format('YYYY-MM-DD'),
    // endDay: 'YYYY-MM-DD',
    endDay: moment().add('2', 'y').format('YYYY-MM-DD'),
    visibleFlag1: false,
    visibleFlag2: false,
    selected1: '2',
    selected2: '5', //이체일
    selected3: 24, //이체일

    totalDay: 0, //전체일수
    waitDay: 0, //근속일수
    minusDay: 0, //남은일수

    nextDay: 0, //다음적립 잔여일
    totalNum: 0, //적립횟수

    endFlag: 'N', //만기일 먼저 입력할 경우 플래그
    nextTransferDay: moment().format('YYYY-MM-DD'),
    lastTransferDay: moment().format('YYYY-MM-DD'),

    selfPay: 0, //본인기여금
    govPay: 0, //정부지원금
    enterPrisePay: 0, //기업기여금

    totalPay: 0, //누적액

    profileImg: null,
  };

  @observable profileValue = '';

  @computed
  get initData() {
    return toJS(this._initData);
  }

  @computed
  get profileValues() {
    return toJS(this.profileValue);
  }

  @action
  dateChagne(dayType, v, selected1) {
    if (dayType === 'startDay') {
      this._initData.startDay = moment(v).format('YYYY-MM-DD');
      if (selected1 !== undefined) {
        if (selected1 === 2 || selected1 === '2') {
          this._initData.endDay = moment(v).add('2', 'y').format('YYYY-MM-DD');
        } else if (selected1 === 3 || selected1 === '3') {
          this._initData.endDay = moment(v).add('3', 'y').format('YYYY-MM-DD');
        } else if (selected1 === 5 || selected1 === '5') {
          this._initData.endDay = moment(v).add('5', 'y').format('YYYY-MM-DD');
        } else {
          this._initData.endDay = moment(v).format('YYYY-MM-DD');
        }
      }
    } else {
      this._initData.endDay = moment(v).format('YYYY-MM-DD');
    }
  }

  @action
  typeChagne(typeChange, v, gubun, selectedDay) {
    if (v === 'date') {
      if (typeChange === 'visibleFlag1') {
        this.gubunFunc(gubun, selectedDay);
        this._initData.visibleFlag1 = false;
      } else {
        this._initData.endDay = moment(selectedDay).format('YYYY-MM-DD');
        this._initData.visibleFlag2 = false;
      }
    } else {
      if (typeChange === 'visibleFlag1') {
        this._initData.visibleFlag1 = true;
      } else {
        //
        if (
          this._initData.startDay !== 'YYYY-MM-DD' &&
          this._initData.startDay !== undefined
        ) {
          this._initData.endFlag = 'N';
          this._initData.visibleFlag2 = true;
        } else {
          this._initData.endFlag = 'Y';
        }
      }
    }
    this.diffDay('selected');
  }

  @action
  diffDay() {
    const {startDay, endDay, selected1, selected2} = this._initData;
    const totalTime = getDiffHours(moment(startDay), moment(endDay));
    const waitTime = getDiffHours(
      moment(startDay),
      moment(moment().format('YYYY-MM-DD')),
    );
    const endTime = getDiffHours(
      moment(endDay),
      moment(moment().format('YYYY-MM-DD')),
    );
    const totalDay = totalTime / 24; //총 D-day
    let waitDay = waitTime / 24; //남은일수
    //let endDay = endTime/24; //남은일수
    if (
      moment(startDay).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD')
    ) {
      waitDay = 1;
    } else if (
      moment(startDay).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD')
    ) {
      waitDay = waitDay + 1;
    } else {
      waitDay = 0;
    }
    //  else if(moment(endDay).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD')){
    //   waitDay = waitDay - 1;
    // }
    //console.log(totalDay, waitDay);
    const minusDay = Number(totalDay) - Number(waitDay);
    var a = moment(startDay);
    var b = moment(moment().format('YYYY-MM-DD'));

    const transferDay = Number(
      moment(moment().format('YYYY-MM'))
        .add(selected2 - 1, 'd')
        .format('DD'),
    ); //이체하는 날
    const nowDay = Number(moment().format('DD')); //현재 일자
    const startDayTmp = Number(moment(startDay).format('DD'));

    const nowTransferDay = moment(moment().format('YYYY-MM'))
      .add(selected2 - 1, 'd')
      .format('YYYY-MM-DD'); //다음달 이체일
    let nextTransferDay = moment(nowTransferDay).format('YYYY-MM-DD'); //실제 이체하는 달
    const lastTransferDay = moment(nowTransferDay).format('YYYY-MM-DD');
    let totalNum = b.diff(a, 'months');

    console.log('요기다>>', Number(transferDay), Number(nowDay));
    if (Number(transferDay) <= Number(nowDay)) {
      totalNum = totalNum + 1;
    }

    console.log('요기다22>>', Number(startDayTmp), Number(nowDay));
    if (startDayTmp >= nowDay) {
      totalNum = totalNum - 1;
    }
    //시작일이 현재일보다 크면 다음달
    if (startDayTmp > nowDay || transferDay <= nowDay) {
      nextTransferDay = moment(nextTransferDay)
        .add('1', 'M')
        .format('YYYY-MM-DD'); //이체 다음달
    }

    //이체하는 날과 현재 일자의 차이 (일수)
    // nextTransferDay b = 현재일자
    let transferTmp = b.diff(nextTransferDay, 'days');

    if (transferTmp < 0) {
      transferTmp = Math.abs(transferTmp);
    }

    let selfPays = 0; //본인납부금
    let govPays = 0; //정부지원금
    let enterPrisePay = 0; //기업기여금

    if (selected1 === 2 || selected1 === '2') {
      //2년형
      if (totalNum > 24) {
        totalNum = 24;
      }
      if (totalNum > 2 && totalNum < 7) {
        // 1,6,12,18,24 => 납부일+1개월씩
        //2021년도 = 1200만원
        if (moment(startDay).format('YYYY') >= 2019) {
          govPays = 800000;
          enterPrisePay = 500000; //기업기여금
        } else {
          govPays = 750000; //정부지원금
          enterPrisePay = 450000; //기업기여금
        }
      } else if (totalNum >= 7 && totalNum < 13) {
        if (moment(startDay).format('YYYY') >= 2021) {
          govPays = 800000 + 1200000; //정부지원금
          enterPrisePay = 500000 + 600000; //기업기여금
        } else {
          govPays = 750000 + 1500000; //정부지원금
          enterPrisePay = 450000 + 700000; //기업기여금
        }
      } else if (totalNum >= 13 && totalNum < 19) {
        if (moment(startDay).format('YYYY') >= 2021) {
          govPays = 800000 + 1200000 + 1200000;
          enterPrisePay = 500000 + 600000 + 600000; //기업기여금
        } else {
          govPays = 750000 + 1500000 + 2250000; //정부지원금
          enterPrisePay = 450000 + 700000 + 950000; //기업기여금
        }
      } else if (totalNum >= 19 && totalNum < 24) {
        if (moment(startDay).format('YYYY') >= 2021) {
          govPays = 800000 + 1200000 + 1200000 + 1400000;
          enterPrisePay = 500000 + 600000 + 600000 + 600000; //기업기여금
        } else {
          govPays = 750000 + 1500000 + 2250000 + 2250000; //정부지원금
          enterPrisePay = 450000 + 700000 + 950000 + 950000;
        }
      } else if (totalNum >= 24) {
        if (moment(startDay).format('YYYY') >= 2021) {
          govPays = 800000 + 1200000 + 1200000 + 1400000 + 1400000;
          enterPrisePay = 500000 + 600000 + 600000 + 600000 + 700000; //기업기여금
        } else {
          govPays = 750000 + 1500000 + 2250000 + 2250000 + 2250000; //정부지원금
          enterPrisePay = 450000 + 700000 + 950000 + 950000 + 950000;
        }
      }
      selfPays = 125000 * totalNum;
      this._initData.selected3 = 24;
    } else if (selected1 === 3 || selected1 === '3') {
      //3년형
      if (totalNum > 36) {
        totalNum = 36;
      }
      selfPays = 165000 * totalNum;
      if (totalNum > 2 && totalNum < 7) {
        // 1,6,12,18,24 => 납부일+1개월씩
        enterPrisePay = 500000; //기업기여금
        govPays = 1500000; //정부지원금
      } else if (totalNum >= 7 && totalNum < 13) {
        enterPrisePay = enterPrisePay + 500000 + 500000;
        govPays = 1500000 + 1750000; //정부지원금
      } else if (totalNum >= 13 && totalNum < 19) {
        enterPrisePay = enterPrisePay + 500000 + 500000 + 750000;
        govPays = 1500000 + 1750000 + 2250000; //정부지원금
      } else if (totalNum >= 19 && totalNum < 25) {
        enterPrisePay = enterPrisePay + 500000 + 500000 + 750000 + 1000000;
        govPays = 1500000 + 1750000 + 2250000 + 2250000; //정부지원금
      } else if (totalNum >= 25 && totalNum < 31) {
        enterPrisePay =
          enterPrisePay + 500000 + 500000 + 750000 + 1000000 + 1000000;
        govPays = 1500000 + 1750000 + 2250000 + 2250000 + 2500000; //정부지원금
      } else if (totalNum >= 31 && totalNum < 36) {
        enterPrisePay =
          enterPrisePay +
          500000 +
          500000 +
          750000 +
          1000000 +
          1000000 +
          1000000;
        govPays = 1500000 + 1750000 + 2250000 + 2250000 + 2500000 + 3250000; //정부지원금
      } else if (totalNum >= 36) {
        enterPrisePay =
          enterPrisePay +
          500000 +
          500000 +
          750000 +
          1000000 +
          1000000 +
          1000000 +
          1250000;
        govPays =
          1500000 + 1750000 + 2250000 + 2250000 + 2500000 + 3250000 + 3500000; //정부지원금
      }
      this._initData.selected3 = 36;
    } else {
      //5년형
      if (totalNum > 60) {
        totalNum = 60;
      }
      selfPays = 120000 * totalNum;
      this._initData.selected3 = 60;
    }
    let totalPays = selfPays + govPays + enterPrisePay;

    this._initData.selfPay = selfPays
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ','); //본인납부금
    this._initData.govPay = govPays
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ','); //정부지원금
    this._initData.enterPrisePay = enterPrisePay
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ','); //기업기여금
    this._initData.totalPay = totalPays
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    this._initData.totalDay = totalDay;
    this._initData.waitDay = waitDay;
    this._initData.minusDay = minusDay;
    this._initData.totalNum = totalNum;
    this._initData.nextDay = transferTmp; //적립일 잔여일
    //
    this._initData.nextTransferDay = nextTransferDay;
    this._initData.lastTransferDay = moment(nextTransferDay)
      .add('-1', 'M')
      .format('YYYY-MM-DD');

    //적립일 D-day 계산 (직전 적립일 ~다음 적립일 을 계산한다.)
    console.log('nextTransferDay>>', nextTransferDay, lastTransferDay);

    function getDiffHours(start, end) {
      return Math.abs(start - end) / 36e5;
    }
  }

  @action
  typeDataChagne(selectedType, data) {
    if (selectedType === 'selected1') {
      if (this._initData.startDay !== 'YYYY-MM-DD') {
        this.gubunFunc(data);
      }
      this._initData.selected1 = data;
    } else {
      this._initData.selected2 = data;
    }
    this.diffDay();
  }

  @action
  profileChange(data) {
    // console.log('실시간>>', data);
    this.profileValue = data;
  }

  @action
  dayFunc(data, v) {
    console.log('데이데이/>>', data, v);
  }

  @action
  setNative(input) {
    console.log('>>>', input);
    // input.current.setNativeProps({ value: 'hello' });
  }

  gubunFunc(data, selectedDay) {
    let day = null;
    if (selectedDay !== undefined) {
      day = selectedDay;
      this._initData.startDay = moment(day).format('YYYY-MM-DD');
    } else {
      day = this._initData.startDay;
    }

    if (data === 2 || data === '2') {
      this._initData.endDay = moment(day).add('2', 'y').format('YYYY-MM-DD');
    } else if (data === 3 || data === '3') {
      this._initData.endDay = moment(day).add('3', 'y').format('YYYY-MM-DD');
    } else if (data === 5 || data === '5') {
      this._initData.endDay = moment(day).add('5', 'y').format('YYYY-MM-DD');
    } else {
      this._initData.endDay = moment(day).add('2', 'y').format('YYYY-MM-DD');
    }
  }

  undefinedChk(data) {
    if (data !== undefined && data !== '' && data !== null) {
      return true;
    } else {
      return false;
    }
  }

  @action
  paramsInit(data) {
    if (this.undefinedChk(data.nickname)) {
      this.profileValue = data.nickname;
    }
    if (this.undefinedChk(data.thumbnail_image)) {
      this._initData.profileImg = data.thumbnail_image;
    }
  }
}
export default new Storage();
