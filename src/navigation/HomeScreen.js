import React, {useState} from 'react';
import {Platform, StyleSheet, Text, View, YellowBox} from 'react-native';
import KakaoLogins from '@react-native-seoul/kakao-login';
import NativeButton from 'apsl-react-native-button';
import {StackActions} from '@react-navigation/native';
import {withNavigation} from 'react-navigation';
import axios from 'axios';
import LottieView from 'lottie-react-native';

const pushAction = StackActions.push('LoginScreen', {user: 'Wojtek'});

if (!KakaoLogins) {
  console.error('Module is Not Linked');
}

const logCallback = (log, callback) => {
  console.log(log);
  callback;
};

const TOKEN_EMPTY = 'token has not fetched';
const PROFILE_EMPTY = {
  id: 'profile has not fetched',
  email: 'profile has not fetched',
  profile_image_url: '',
};

function HomeScreen({navigation}) {
  const [loginLoading, setLoginLoading] = useState(false);
  const [logoutLoading, setLogoutLoading] = useState(false);
  const [profileLoading, setProfileLoading] = useState(false);
  const [unlinkLoading, setUnlinkLoading] = useState(false);

  const [token, setToken] = useState(TOKEN_EMPTY);
  const [profile, setProfile] = useState(PROFILE_EMPTY);

  const [propertiesManage, setProperties] = useState('');

  async function getPost(accessToken) {
    await axios({
      method: 'GET',
      url: 'https://kapi.kakao.com/v2/user/me?secure_resource=true',
      headers: {
        Host: 'kapi.kakao.com',
        Authorization: `Bearer ${accessToken}`,
        'Content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
    })
      .then((response) => {
        const {data} = response;
        //console.log('액세스토큰>> ',accessToken);
        setProperties(data.properties);
        //console.log('띠용>>>',data.properties);
        navigation.jumpTo('LoginScreen', {properties: data.properties});
        //id가 고유키, 이메일(선택항목), 닉네임, --> post
        //먼저 조회를 해서 id가 있으면 return 없으면 post
      })
      .catch((error) => console.log('에러>>>', error));
  }

  const kakaoLogin = () => {
    logCallback('Login Start', setLoginLoading(true));
    KakaoLogins.login()
      .then((result) => {
        setToken(result.accessToken);
        getPost(result.accessToken);
        // console.log('>>>',result);
        logCallback(
          //console.log('>>>',propertiesManage),
          `Login Finished:${JSON.stringify(result)}`,
          // navigation.jumpTo('LoginScreen');
          setLoginLoading(false),
        );
      })
      .catch((err) => {
        if (err.code === 'E_CANCELLED_OPERATION') {
          logCallback(`Login Cancelled:${err.message}`, setLoginLoading(false));
        } else {
          logCallback(
            `Login Failed:${err.code} ${err.message}`,
            setLoginLoading(false),
          );
        }
      });
  };

  const kakaoLogout = () => {
    logCallback('Logout Start', setLogoutLoading(true));

    KakaoLogins.logout()
      .then((result) => {
        setToken(TOKEN_EMPTY);
        //setProfile(PROFILE_EMPTY);
        logCallback(`Logout Finished:${result}`, setLogoutLoading(false));
      })
      .catch((err) => {
        logCallback(
          `Logout Failed:${err.code} ${err.message}`,
          setLogoutLoading(false),
        );
      });
  };

  const getProfile = () => {
    logCallback('Get Profile Start', setProfileLoading(true));

    KakaoLogins.getProfile()
      .then((result) => {
        //console.log('get>>>',result);
        //setProfile(result);
        logCallback(
          `Get Profile Finished:${JSON.stringify(result)}`,
          setProfileLoading(false),
        );
      })
      .catch((err) => {
        logCallback(
          `Get Profile Failed:${err.code} ${err.message}`,
          setProfileLoading(false),
        );
      });
  };

  // const unlinkKakao = () => {
  //   logCallback('Unlink Start', setUnlinkLoading(true));

  //   KakaoLogins.unlink()
  //     .then((result) => {
  //       setToken(TOKEN_EMPTY);
  //       setProfile(PROFILE_EMPTY);
  //       logCallback(`Unlink Finished:${result}`, setUnlinkLoading(false));
  //     })
  //     .catch((err) => {
  //       logCallback(
  //         `Unlink Failed:${err.code} ${err.message}`,
  //         setUnlinkLoading(false),
  //       );
  //     });
  // };

  const {id, email, profile_image_url: photo} = profile;
  return (
    <View style={styles.container}>
      <View style={styles.profile}>
        <LottieView
          source={require('../lottie/3169-world.json')}
          autoPlay
          loop
          //
        />
        {/* <Image style={styles.profilePhoto} source={{uri: photo}} /> */}
        {/* <Text>{`id : ${id}`}</Text>
              <Text>{`email : ${email}`}</Text> */}
      </View>
      <View style={styles.content}>
        <Text style={styles.token}>{'런위드미'}</Text>
        <NativeButton
          isLoading={loginLoading}
          onPress={kakaoLogin}
          activeOpacity={0.5}
          style={styles.btnKakaoLogin}
          textStyle={styles.txtKakaoLogin}>
          KAKAO LOGIN
        </NativeButton>
        {/* <NativeButton
                isLoading={logoutLoading}
                onPress={kakaoLogout}
                activeOpacity={0.5}
                style={styles.btnKakaoLogin}
                textStyle={styles.txtKakaoLogin}>
                Logout
              </NativeButton> */}
        {/* <NativeButton
                isLoading={profileLoading}
                onPress={getProfile}
                activeOpacity={0.5}
                style={styles.btnKakaoLogin}
                textStyle={styles.txtKakaoLogin}>
                getProfile
              </NativeButton>
              <NativeButton
                isLoading={unlinkLoading}
                onPress={unlinkKakao}
                activeOpacity={0.5}
                style={styles.btnKakaoLogin}
                textStyle={styles.txtKakaoLogin}>
                unlink
              </NativeButton> */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: Platform.OS === 'ios' ? 0 : 24,
    paddingTop: Platform.OS === 'ios' ? 24 : 0,
    backgroundColor: 'white',
  },
  profile: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  profilePhoto: {
    width: 120,
    height: 120,
    borderWidth: 1,
    borderColor: 'black',
  },
  content: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  token: {
    width: 200,
    fontSize: 12,
    padding: 5,
    borderRadius: 8,
    marginVertical: 10,
    backgroundColor: 'grey',
    color: 'white',
    textAlign: 'center',
  },
  btnKakaoLogin: {
    height: 48,
    width: 240,
    alignSelf: 'center',
    backgroundColor: '#F8E71C',
    // borderRadius: 0,
    // borderWidth: 0,
  },
  txtKakaoLogin: {
    fontSize: 16,
    color: '#3d3d3d',
  },
});
YellowBox.ignoreWarnings(['source.uri']);

//export default HomeScreen;
export default withNavigation(HomeScreen);
