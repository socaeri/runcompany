import React, {Component} from 'react';
import {
  Modal,
  View,
  StyleSheet,
  StatusBar,
  YellowBox,
  Alert,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {observer, inject} from 'mobx-react';
import {
  Content,
  Text,
  Toast,
  Root,
  Card,
  CardItem,
  Left,
  Body,
  Right,
} from 'native-base';
import moment from 'moment';
import {Header, Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {withNavigation} from 'react-navigation';
import {DatePicker} from 'react-native-common-date-picker';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUserCircle} from '@fortawesome/free-solid-svg-icons';
import _ from 'lodash';

const input = React.createRef();

YellowBox.ignoreWarnings(['componentWillReceiveProps']);
const _console = _.clone(console);
console.warn = (message) => {
  if (message.indexOf('componentWillReceiveProps') <= -1) {
    _console.warn(message);
  }
};

@inject('storage')
@observer
class LoginScreen extends Component {
  componentDidMount() {
    const {params} = this.props.route;
    YellowBox.ignoreWarnings(['Animated: `useNativeDriver`']);
    this.props.storage.paramsInit(params.properties); //로그인 후 최초의 parameter 담을 공간
  }

  setDate(dayType, v) {
    this.props.storage.dateChagne(dayType, v);
  }

  setTypeFlag(type, v, gubun, selectedDay) {
    this.props.storage.typeChagne(type, v, gubun, selectedDay);
  }

  setTypeData(selectedType, data) {
    this.props.storage.typeDataChagne(selectedType, data);
  }

  profileChange(type, v) {
    //input.current.setNativeProps({ value: 'hello' });
    this.props.storage.profileChange(type, v);
  }

  goAlert = () => {
    const {initData, profileValues} = this.props.storage;
    Alert.alert(
      '[런위드미]', // 첫번째 text: 타이틀 제목
      '설정을 저장하시겠습니까?', // 두번째 text: 그 밑에 작은 제목
      [
        // 버튼 배열
        {
          text: '아니요', // 버튼 제목
          onPress: () => console.log('아니라는데'), //onPress 이벤트시 콘솔창에 로그를 찍는다
          style: 'cancel',
        },
        {
          text: '네',
          onPress: () =>
            this.props.navigation.navigate('CalculateScreen', {
              selected1: initData.selected1,
              selected2: initData.selected2,
              startDay: initData.startDay,
              endDay: initData.endDay,
              profileValue: profileValues,
              totalDay: initData.totalDay,
              waitDay: initData.waitDay,
              minusDay: initData.minusDay,
              totalNum: initData.totalNum,
              selected3: initData.selected3,
              nextDay: initData.nextDay,
              nextTransferDay: initData.nextTransferDay,
              lastTransferDay: initData.lastTransferDay,
              profileImg: initData.profileImg,
              selfPay: initData.selfPay,
              govPay: initData.govPay,
              enterPrisePay: initData.enterPrisePay,
              totalPay: initData.totalPay,
            }),
        },
      ],
      {cancelable: false},
    );
  };

  render() {
    // const {params} = this.props.route;
    const {initData, profileValues} = this.props.storage;
    // console.log('params>>', params);

    return (
      <Root>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" />
          <Header
            placement="left"
            leftComponent={{
              icon: 'menu',
              color: '#212121',
              onPress: () => this.props.navigation.openDrawer(),
            }}
            centerComponent={{
              text: '설정',
              style: {color: '#212121'},
            }}
            rightComponent={{icon: 'home', color: '#212121'}}
            backgroundColor={'#ffffff'}
          />
          <Content>
            <Card style={{flex: 0}}>
              <CardItem>
                <Input
                  label="PROFILE"
                  placeholder="NICKNAME"
                  ref={input}
                  leftIcon={
                    <FontAwesomeIcon
                      icon={faUserCircle}
                      size={24}
                      color={'black'}
                    />
                  }
                  value={this.props.storage.profileValues}
                  onChangeText={this.profileChange.bind(this)}
                  errorStyle={{color: 'red'}}
                  errorMessage={'* 별명(닉네임)을 입력해주세요'}
                />
              </CardItem>

              <CardItem>
                <Body>
                  <Left style={styles.colStyle}>
                    <Text>유형선택</Text>
                    <Picker
                      mode="dropdown"
                      //placeholder="Select your SIM"
                      placeholderStyle={{color: '#bfc6ea'}}
                      //placeholderIconColor="#007aff"
                      style={{width: 200}}
                      selectedValue={initData.selected1}
                      onValueChange={this.setTypeData.bind(this, 'selected1')}>
                      <Picker.Item label="2년형" value="2" />
                      <Picker.Item label="3년형" value="3" />
                      <Picker.Item label="5년형" value="5" />
                    </Picker>
                  </Left>
                </Body>
              </CardItem>

              <CardItem>
                <Body>
                  <Left style={styles.colStyle}>
                    <Text>공제 시작일</Text>
                    <Modal
                      animationType={'slide'}
                      transparent={true}
                      visible={initData.visibleFlag1}
                      onRequestClose={() => {}}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'column',
                          justifyContent: 'flex-end',
                        }}>
                        <DatePicker
                          confirm={this.setTypeFlag.bind(
                            this,
                            'visibleFlag1',
                            'date',
                            initData.selected1,
                          )}
                          toolBarStyle={{backgroundColor: '#f8f8ff'}}
                          toolBarConfirmStyle={{color: 'black'}}
                          defaultDate={moment().format('YYYY-MM-DD')}
                          minDate={'2017-01-01'}
                          maxDate={'2050-12-31'}
                          onValueChange={this.setDate.bind(
                            this,
                            'startDay',
                            initData.selected1,
                          )}
                          confirmText="선택"
                          cancelText="취소"
                        />
                      </View>
                    </Modal>
                    <Button
                      icon={<Icon name="calendar" size={25} color="gray" />}
                      buttonStyle={{
                        justifyContent: 'flex-start',
                      }}
                      iconRight
                      title={initData.startDay + '\t\t'}
                      onPress={this.setTypeFlag.bind(
                        this,
                        'visibleFlag1',
                        'button',
                      )}
                      type="clear"
                      // disabledTitleStyle
                    />
                  </Left>
                </Body>
              </CardItem>

              <CardItem>
                <Body>
                  <Left style={styles.colStyle}>
                    <Text>만기일</Text>
                    <Modal
                      animationType={'slide'}
                      transparent={true}
                      visible={initData.visibleFlag2}
                      onRequestClose={() => {}}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'column',
                          justifyContent: 'flex-end',
                        }}>
                        <DatePicker
                          confirm={this.setTypeFlag.bind(
                            this,
                            'visibleFlag2',
                            'date',
                            initData.selected1,
                          )}
                          toolBarStyle={{backgroundColor: '#f8f8ff'}}
                          toolBarConfirmStyle={{color: 'black'}}
                          onValueChange={this.setDate.bind(this, 'endDay')}
                          defaultDate={initData.endDay}
                          minDate={moment().format('YYYY-MM-DD')}
                          maxDate="2050-12-31"
                          confirmText="선택"
                          cancelText="취소"
                        />
                      </View>
                    </Modal>
                    <Button
                      icon={<Icon name="calendar" size={25} color="gray" />}
                      buttonStyle={{
                        justifyContent: 'flex-start',
                      }}
                      iconRight
                      title={initData.endDay + '\t\t'}
                      onPress={this.setTypeFlag.bind(
                        this,
                        'visibleFlag2',
                        'button',
                      )}
                      type="clear"
                    />
                  </Left>
                </Body>
              </CardItem>

              <CardItem>
                <Body>
                  <Left style={styles.colStyle}>
                    <Text>납입 일자</Text>
                    <Picker
                      mode="dropdown"
                      //iosIcon={<Icon name="arrow-down" />}
                      placeholder="Select your SIM"
                      placeholderStyle={{color: '#bfc6ea'}}
                      placeholderIconColor="#007aff"
                      style={{width: 200}}
                      selectedValue={initData.selected2}
                      //onValueChange={this.onValueChange.bind(this, 'selected2')}
                      onValueChange={this.setTypeData.bind(this, 'selected2')}>
                      <Picker.Item label="5일" value="5" />
                      <Picker.Item label="15일" value="15" />
                      <Picker.Item label="25일" value="25" />
                    </Picker>
                  </Left>
                </Body>
              </CardItem>
            </Card>
          </Content>
          <Button
            title="설정완료"
            onPress={() => {
              this.props.storage.diffDay();
              if (profileValues === '' || profileValues === undefined) {
                Toast.show({
                  text: '프로필명을 입력해주세요!',
                  buttonText: 'Okay',
                });
              } else if (
                initData.startDay === '' ||
                initData.startDay === undefined ||
                initData.startDay === 'YYYY-MM-DD'
              ) {
                Toast.show({
                  text: '시작일을 입력해주세요!',
                  buttonText: 'Okay',
                });
              } else if (
                initData.endDay === '' ||
                initData.endDay === undefined ||
                initData.endDay === 'YYYY-MM-DD'
              ) {
                Toast.show({
                  text: '만기일을 입력해주세요!',
                  buttonText: 'Okay',
                });
              } else {
                this.goAlert();
                // this.props.navigation.navigate('CalculateScreen', {
                //   selected1: initData.selected1,
                //   selected2: initData.selected2,
                //   startDay: initData.startDay,
                //   endDay: initData.endDay,
                //   profileValue: profileValues,
                //   totalDay: initData.totalDay,
                //   waitDay: initData.waitDay,
                //   minusDay: initData.minusDay,
                //   totalNum: initData.totalNum,
                //   selected3: initData.selected3,
                //   nextDay: initData.nextDay,
                //   nextTransferDay: initData.nextTransferDay,
                //   lastTransferDay: initData.lastTransferDay,
                //   profileImg: initData.profileImg,
                // });
              }
            }}
          />
        </View>
      </Root>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  modalContent: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
  },
  alertStyle: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  colStyle: {
    // flex: 1,
    // flexDirection: 'column', // 혹은 'column'
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#eee',
    borderBottomWidth: 0.5,
    padding: 5,
  },
});
export default withNavigation(LoginScreen);
