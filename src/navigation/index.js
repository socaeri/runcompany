import React from 'react';
import LoginScreen from './LoginScreen';
import CalculateScreen from './CalculateScreen';
import HomeScreen from './HomeScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

function RootStack() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="로그인" component={HomeScreen} />
      <Drawer.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{drawerLabel: '설정'}}
      />
      <Drawer.Screen
        name="CalculateScreen"
        component={CalculateScreen}
        options={{drawerLabel: '목표달성'}}
      />
    </Drawer.Navigator>
  );
}

export default RootStack;
