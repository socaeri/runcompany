import React, {Component} from 'react';
import {View, StyleSheet, StatusBar, Image} from 'react-native';
import {observer, inject} from 'mobx-react';
import {
  Content,
  Text,
  Button,
  Card,
  CardItem,
  Left,
  Body,
  Right,
} from 'native-base';
import {Svg, Rect} from 'react-native-svg';
import {Header} from 'react-native-elements';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCrown, faUserCircle, faCog} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import {withNavigation} from 'react-navigation';
import BackgroundTimer from './BackgroundTimer';
//import { YellowBox } from 'react-native';

//import 'moment/locale/ko';
//import { useInterval } from 'react-use';
// const intervalId = BackgroundTimer.setInterval(() => {
// 	// this will be executed every 200 ms
// 	// even when app is the the background
// 	console.log('tic');
// }, 200);

// // Cancel the timer when you are done with it
// BackgroundTimer.clearInterval(intervalId);
// _interval: any;

@inject('storage')
@observer
class CalculateScreen extends Component {
  componentDidMount() {
    this.onStart(this, 'start');
    // this.onStart(this);
    //YellowBox.ignoreWarnings(['Animated: `useNativeDriver`']);
  }
  componentWillUnmount() {
    // if(this.interval){
    //     clearInterval(this.interval)
    // }
  }

  // static defaultProps: any;
  constructor(props) {
    super(props);
    this.state = {
      second: 0,
      width: 354,
    };
  }

  onStart(_this, validId) {
    let intervalId = null;
    if (validId === 'start') {
      intervalId = BackgroundTimer.setInterval(() => {
        _this.setState({second: _this.state.second + 1});
      }, 100);
    } else {
      BackgroundTimer.clearInterval(intervalId);
    }
  }

  // Start a timer that runs continuous after X milliseconds
  // const intervalId = BackgroundTimer.setInterval(() => {
  //     // this will be executed every 200 ms
  //     // even when app is the the background
  //     console.log('tic');
  // }, 200);

  // // Cancel the timer when you are done with it

  dayFunc(dayType, v) {
    this.props.storage.dayFunc(dayType, v);
  }

  calAction(datas) {
    const {params} = this.props.route;

    let startDay = params.startDay;
    let endDay = params.endDay;

    if (datas !== 'mainData') {
      startDay = params.lastTransferDay;
      //  params.nextTransferDay
      //
      endDay = params.nextTransferDay;
      //console.log('nonono>', startDay, endDay);
    }
    //console.log('route이후>>>',params);
    // new Date(2017, 7, 23, 9), new Date(2017, 7, 24, 9)
    // var diffTraker = new DiffTracker(moment(params.startDay), moment(params.endDay));
    var diffTraker = new DiffTracker(moment(startDay), moment(endDay));
    /////////
    // new Date(2017, 7, 23, 21)
    //현재기준 퍼센트

    return diffTraker.getPercentage(moment());
    //console.log(diffTraker.getPercentage(moment()) + '%');
    // console.log(diffTraker.getPercentage(moment(params.startDay).add("5", "h")) + '% from start date (' + moment(params.startDay).format('YYYY-MM-DD HH:mm:ss') + ')');
    // console.log(diffTraker.getPercentage(moment(params.startDay).add("12", "h")) + '% from start date (' + moment(params.startDay).format('YYYY-MM-DD HH:mm:ss') + ')');
    // console.log(diffTraker.getPercentage(moment(params.startDay).add("24", "h")) + '% from start date (' + moment(params.startDay).format('YYYY-MM-DD HH:mm:ss') + ')');

    function DiffTracker(startDate, endDate) {
      var self = this;
      self.start = startDate;
      self.end = endDate;
      self.totalHours = getDiffHours(self.start, self.end);

      //const totalDay = (self.totalHours)/24;

      //() => dayFunc.bind(totalDay);
      //console.log('hour>>', (self.totalHours)/24);

      self.getPercentage = function (date) {
        //console.log('>>>>>>>>',moment(self.start).format('YYYY-MM-DD') ==  moment(self.end).format('YYYY-MM-DD'), self.start, self.end);

        var hoursFromStart = 0;
        if (
          moment(self.start).format('YYYY-MM-DD') ==
            moment(self.end).format('YYYY-MM-DD') ||
          self.start <= date
        ) {
          hoursFromStart = getDiffHours(self.start, date);
        }
        //toFixed는 추후 변경, 실시간 퍼센트 계산
        let returnData = ((hoursFromStart * 100) / self.totalHours).toFixed(7);
        //.toFixed(2);
        if (returnData > 100) {
          returnData = 100;
        }
        return returnData;
      };

      function getDiffHours(start, end) {
        /* 36e5 is the scientific notation for 60*60*1000, dividing by which converts the milliseconds difference into hours */
        return Math.abs(start - end) / 36e5;
      }
    }
    /////////
  }

  render() {
    const {params} = this.props.route;
    // const { profileValue } = this.props.route;
    const {width} = this.state;
    //console.log('params>>', params.startDay , params.endDay);
    //

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Header
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#212121',
            onPress: () => {
              //this.onStart(this, 'stop')
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{text: '목표달성', style: {color: '#212121'}}}
          rightComponent={{
            icon: 'settings',
            color: '#212121',
            onPress: () => {
              this.props.navigation.navigate('LoginScreen');
            },
          }}
          backgroundColor={'#ffffff'}
        />
        <Content>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                {params.profileImg !== '' &&
                params.profileImg !== undefined &&
                params.profileImg !== null ? (
                  <Image
                    style={styles.tinyLogo}
                    source={{
                      uri: params.profileImg,
                    }}
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={faUserCircle}
                    size={60}
                    color={'gray'}
                  />
                )}

                {/* <Thumbnail source={{uri: '../kakaotalk-logo-card-2018'}} /> */}
                <Body>
                  <Text>{params.profileValue}</Text>
                  <Text note>{'시작일 ' + params.startDay}</Text>
                  <Text note>{'종료일 ' + params.endDay}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              {/* style={{height: 200, width: null, flex: 1}} */}
              <Svg width="380" height="40">
                <Rect
                  x="20"
                  y="3"
                  width={width}
                  height="30"
                  fill="rgb(240,235,229)"
                  strokeWidth="1"
                  stroke="rgb(125,116,111)"
                />
                <Rect
                  x="20"
                  y="3"
                  width={
                    Number(width) * Number(this.calAction('mainData') / 100)
                  }
                  height="30"
                  fill="rgb(255,143,143)"
                  strokeWidth="1"
                  stroke="rgb(125,116,111)"
                  //123,184,179 --mint
                  // 255,143,143 --pink
                />
              </Svg>
            </CardItem>
            <CardItem>
              <Left>
                <Text>
                  {Math.ceil(params.waitDay) + ' / ' + params.totalDay + '일'}
                </Text>
              </Left>
              <Right>
                <Text>
                  {this.calAction('mainData') + '%'}
                  {/* {params.totalDay} */}
                </Text>
              </Right>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#afc2c6'}}>
                  <FontAwesomeIcon icon={faCrown} size={24} color={'#febe2e'} />
                  <Text>
                    {'TOTAL  ' +
                      params.totalDay +
                      '일\nD-day  ' +
                      Math.floor(params.minusDay) +
                      '일'}
                  </Text>
                </Button>
                <Right>
                  <Button
                    rounded
                    textStyle={{color: '#afc2c6'}}
                    // onPress={() => this.onStart(this, 'start')}
                  >
                    <Text>
                      {'적립 횟수 : ' +
                        params.totalNum +
                        ' / ' +
                        params.selected3 +
                        '회'}
                    </Text>
                  </Button>
                </Right>
                {/* <Text>{"D - "+params.minusDay+"일"}</Text> */}
              </Left>
            </CardItem>
          </Card>
          <Card>
            <CardItem header>
              <Left>
                <Text>{params.totalNum + 1 + '회차 적립일'}</Text>
              </Left>
            </CardItem>
            <CardItem>
              <Text
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {moment(params.lastTransferDay).format('MM/DD')}
              </Text>
              <Text>{moment(params.nextTransferDay).format('MM/DD')}</Text>
            </CardItem>
            <CardItem cardBody>
              <Left>
                <Body>
                  <Svg width="380" height="40">
                    <Rect
                      x="20"
                      y="3"
                      width={width}
                      height="30"
                      fill="rgb(240,235,229)"
                      strokeWidth="1"
                      stroke="rgb(125,116,111)"
                    />
                    <Rect
                      x="20"
                      y="3"
                      width={
                        Number(width) *
                        Number(this.calAction('otherData') / 100)
                      }
                      height="30"
                      fill="rgb(123,184,179)"
                      strokeWidth="1"
                      stroke="rgb(125,116,111)"
                      //123,184,179 --mint
                      // 255,143,143 --pink
                    />
                  </Svg>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Left>
                <Text>{'D - ' + params.nextDay + '일'}</Text>
              </Left>
              <Right>
                <Text>{this.calAction('otherData') + '%'}</Text>
              </Right>
            </CardItem>
          </Card>
          <Card style={{height: 140}}>
            <CardItem>
              <Left>
                <Text style={{color: '#423f3f', fontSize: 13}}>
                  {'본인납부금\t\t' + params.selfPay + ' 원\n'}
                  {'정부지원금\t\t' + params.govPay + ' 원\n'}
                  {'기업납부금\t\t' + params.enterPrisePay + ' 원'}
                </Text>
              </Left>
              <Right>
                <Button
                  style={{width: 200}}
                  // textStyle={{color: '#afc2c6', width:400}}
                  rounded
                  // onPress={() => this.onStart(this, 'start')}
                >
                  <Text
                    style={{
                      textAlign: 'right',
                    }}>
                    {'적립금액 : ' + params.totalPay + '원'}
                  </Text>
                </Button>
              </Right>
            </CardItem>
            <Left>
              <Text
                style={{color: '#817f7f', fontSize: 10, textAlign: 'center'}}>
                {
                  '※위 적립 금액은 실제 내일채움공제 홈페이지 금액과 다를 수 있습니다.※\n ※자세한 적립 금액은 내일채움공제 홈페이지를 참고 바랍니다※'
                }
              </Text>
            </Left>
          </Card>
        </Content>
      </View>
    );
  }
  //
  // const timer = BackgroundTimer.setInterval(callback, 1000);
  // BackgroundTimer.clearInterval(timer)
  //     onStart(_this) {
  //         BackgroundTimer.setInterval(_this.setState({second :  _this.state.second + 1}), 1000);
  //         // _interval = setInterval(() => {
  //         //     _this.setState({second :  _this.state.second + 1})
  //         // }, 500)
  //    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: 'white',
  },
  logo: {
    width: 76,
    height: 68,
  },
});

// export default CalculateScreen;
export default withNavigation(CalculateScreen);
